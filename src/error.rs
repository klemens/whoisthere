use ssh2;
use std::io;
use std::result;

#[derive(Debug)]
pub(crate) enum Error {
    Io(io::Error),
    Ssh(ssh2::Error),
    Timeout,
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::Io(error)
    }
}
impl From<ssh2::Error> for Error {
    fn from(error: ssh2::Error) -> Self {
        Error::Ssh(error)
    }
}

pub(crate) type Result<T> = result::Result<T, Error>;