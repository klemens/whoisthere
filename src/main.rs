extern crate futures;
extern crate futures_cpupool;
#[macro_use] extern crate lazy_static;
extern crate tokio_core;
extern crate trust_dns_resolver;
extern crate shio;
extern crate ssh2;

mod error;
mod future_combinators;

use error::*;
use futures::Future;
use futures::future::{Either, err, ok, result};
use futures_cpupool::CpuPool;
use future_combinators::join_ok;
use shio::prelude::*;
use ssh2::Session;
use std::io::Read;
use std::net;
use std::path::Path;
use std::time::Duration;
use tokio_core::net::TcpStream;
use tokio_core::reactor::Timeout;
use trust_dns_resolver::ResolverFuture;

// Should be removed when shio supports shared state
lazy_static! {
    static ref POOL: CpuPool = CpuPool::new(10);
    static ref PEERS: Vec<(String, String, u16)> = vec![
        ("klemens".into(), "localhost".into(), 22),
    ];
}

fn main() {
    Shio::default()
        .route((Method::Get, "/", find_logins))
        .run(":7878")
        .unwrap();
}

fn find_logins(ctx: Context) -> BoxFuture<Response, Error> {
    let mut resolver = ResolverFuture::from_system_conf(ctx.handle()).unwrap();

    let futures = PEERS.iter().map(move |&(ref username, ref host, port)| {
        let ip = resolver.lookup_ip(host)
            .map_err(Error::from);

        let handle = ctx.handle().clone();
        let connection = ip.and_then(move |ips| {
            let ip = ips.iter().next().unwrap();
            let sock_addr = net::SocketAddr::new(*ip, port);
            TcpStream::connect(&sock_addr, &handle)
                .from_err()
        });
        let timeout = Timeout::new(Duration::from_millis(700), ctx.handle())
            .expect("Could not create Timeout")
            .from_err();

        connection.select2(timeout).map_err(|either| {
            // split out the common Error
            either.split().0
        }).and_then(move |selected| {
            match selected {
                Either::A((conn, _timeout)) => ok(conn),
                Either::B((_timeout, _conn)) => err(Error::Timeout),
            }
        }).and_then(move |connection| {
            POOL.spawn_fn(move || {
                use std::os::unix::io::AsRawFd;
                use std::os::unix::io::FromRawFd;

                let fd = connection.as_raw_fd();
                let stream = unsafe {
                    // This is safe, because connection outlives stream
                    net::TcpStream::from_raw_fd(fd)
                };

                result(check_ssh(&stream, username)
                    .map(|result| (host, result)))
            })
        })
    });

    join_ok(futures)
        .and_then(|online_peers| {
            let mut result = "<pre>\n".to_string();
            for (_host, who) in online_peers {
                result.push_str(&who);
                result.push_str("\n");
            }
            Ok(Response::build().body(result))
        })
        .into_box()
}

fn check_ssh(stream: &net::TcpStream, username: &str) -> Result<String> {
    let mut session = Session::new()
        .ok_or(ssh2::Error::unknown())?;

    session.set_timeout(1000);

    session.handshake(&stream)?;
    session.userauth_pubkey_file(username, Some(Path::new("key.pub")),
        &Path::new("key"), None)?;

    let mut channel = session.channel_session()?;
    channel.exec("who")?;
    let mut who = String::new();
    channel.read_to_string(&mut who)?;

    Ok(who)
}
